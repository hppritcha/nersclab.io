# Genepool

<img style="float: right;" alt="Genepool logo"
src="../../img/Genepool-logo.jpg"> The Genepool is a "metacluster" of
computing resources dedicated to the DOE Joint Genome Institute's
bioinformatics needs.

Currently, "Genepool" includes resources from two clusters: The Denovo
system, which runs on the Mendel cluster, and a dedicated partition on
Cori. The name "genepool" also refers to a no-longer-used system that
ran on Mendel.

## [Getting started](genepool/getting-started.md)

Learn how to log in and get started with Genepool. The Genepool system will be
made up of a heterogeneous collection of nodes to serve the diverse workload
of the JGI users.

## [Software](genepool/genepool-software.md)

A lot of software is available via software modules or
anaconda. Details regarding software maintenance and access can be
found here.

## [Queues and Policies](genepool/genepool-queues-and-policies.md)

Learn about the fair share batch scheduler and queues used on
Genepool.

## [Cluster Statistics](genepool/genepool-cluster-statistics.md)

Basic user and group usage statistiscs.

## [File storage and I/O](genepool/genepool-file-storage-and-io.md)

There are 5 main areas on Genepool where users can read, write and
store data. This page documents the purpose and performance of each
one.

## [Data Management](genepool/genepool-data-management.md)

Where to store your data, how to make efficient use of the
filesystems, how to backup your important data to tape.

## [Genepool Training and Tutorials](genepool/genepool-training-and-tutorials.md)

This is a summary page for the trainings and tutorial sessions that
have been given at the JGI.

## [Websites, databases and cluster services](genepool/genepool-websites-databases-and-cluster-services.md)

Regulations and best practices for running and using network services.

## [Collaboration Accounts](genepool/collab-accounts.md)

Information about using collaboration accounts.

## [Denovo, the replacement for Genepool](genepool/genepool-denovo-the-replacement-for-genepool.md)

Denovo is the upgraded version of Genepool, and is in production use
now.

## [Cori for JGI](genepool/genepool-cori-for-jgi.md)

Cori, NERSC's biggest supercomputer, is also available for use by JGI
users. Burst buffer, Shifter, and all other features available to
Haswell Cori nodes are available via a new JGI "quality of service"
(QOS).
