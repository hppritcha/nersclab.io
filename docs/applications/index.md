# Applications

NERSC provides [modules](docs/environment/#nersc-modules-environment)
for a variety of common science codes such as:

* [VASP](vasp/index.md)
* [Quantum ESPRESSO](quantum-espresso/index.md)
* [NAMD](namd/index.md)
