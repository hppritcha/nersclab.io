NetCDF (Network Common Data Form) is a set of software libraries and
machine-independent data formats that support the creation, access,
and sharing of array-oriented scientific data.  This includes the
libnetcdf.a library as well as the NetCDF Operators (NCO), Climate
Data Operators (CDO), NCCMP, and NCVIEW packages.
