# Edison

Edison is a Cray XC30 with a peak performance of more than 2
petaflops. Edison features the Cray Aries high-speed interconnect,
fast Intel Xeon processors, and 64 GB of memory per node.

## Filesystems

* [Edison scratch](/filesystems/edison-scratch.md)
* [Cori scratch](/filesystems/cori-scratch.md)[^1]

[^1]: Cori's scratch filesystem is also mounted on Edison login and
      compute nodes
